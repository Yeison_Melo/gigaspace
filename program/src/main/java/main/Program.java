package main;


/**
 * @author Yeison Melo {@literal <mailto:melo.yeison@gmail.com/>}
 */

import com.j_spaces.core.client.SQLQuery;
import domain.Friendship;
import domain.User;
import org.openspaces.core.GigaSpace;
import org.openspaces.core.GigaSpaceConfigurer;
import org.openspaces.core.space.SpaceProxyConfigurer;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Program {

    public static void main(String[] args) {

        GigaSpace gigaSpace = getEmbedded();

        System.out.println("Write (store) a couple of entries in the data grid:");
        gigaSpace.write(new User(1L, "Vincent Melo", "Chase", 25));
        gigaSpace.write(new User(2L, "Johnny Acosta", "Drama", 23));
        gigaSpace.write(new Friendship(1L, 2L));

        System.out.println("Read (retrieve) an entry from the grid by its id:");
        User result1 = gigaSpace.readById(User.class, 1);
        System.out.println("Result: " + result1);

        System.out.println("Read an entry from the grid using a SQL-like query:");
        User result2 = gigaSpace.read(new SQLQuery<User>(User.class, "firstName=?", "Johnny"));
        System.out.println("Result: " + result2);


        System.out.println("Read all entries of type User from the grid:");
        User[] results = gigaSpace.readMultiple(new User());
        System.out.println("Result: " + java.util.Arrays.toString(results));


    }

    private static GigaSpace getEmbedded()  {
        FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext(
                "classpath:/spring/application-context.xml");

        return  (GigaSpace) context.getBean("meloSpace");
    }

    private static GigaSpace getRemote(){
        System.out.println("Connecting to data grid");
        SpaceProxyConfigurer configurer = new SpaceProxyConfigurer("meloSpace");
        configurer.lookupGroups("xap-12.1.0");
        //configurer.close();
        return new GigaSpaceConfigurer(configurer).create();
    }
}
