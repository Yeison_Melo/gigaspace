package domain;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

/**
 * @author Yeison Melo {@literal <mailto:melo.yeison@gmail.com/>}
 * Represents the friendship between to users, this class is a nice
 * example of how to use another class as space id.
 */

@SpaceClass
public class Friendship {

    FriendshipCompoundId id;

    public  Friendship() {}
    public Friendship(Long idUserOne, Long idUserTwo) {
        id = new FriendshipCompoundId (idUserOne, idUserTwo);
    }

    @SpaceId(autoGenerate = false)
    public FriendshipCompoundId getId() {
        return id;
    }

    public void setId(FriendshipCompoundId id) {
        this.id = id;
    }
}
