package domain;

import java.io.Serializable;

/**
 * @author Yeison Melo {@literal <mailto:melo.yeison@gmail.com/>}
 *
 * To simplify this class we ensure that the first element is always the smaller
 * of the couple. So we don't have to worry of permutations like 2-1 1-2
 */

public class FriendshipCompoundId implements Serializable{

    Long idUserOne;
    Long idUserTwo;

    public FriendshipCompoundId(Long idUserOne, Long idUserTwo) {
        if(idUserOne<idUserTwo) {
            this.idUserOne = idUserOne;
            this.idUserTwo = idUserTwo;
        }else{
            this.idUserOne = idUserTwo;
            this.idUserTwo = idUserOne;
        }
    }

    public Long getIdUserOne() {
        return idUserOne;
    }

    public void setIdUserOne(Long idUserOne) {
        this.idUserOne = idUserOne;
    }

    public Long getIdUserTwo() {
        return idUserTwo;
    }

    public void setIdUserTwo(Long idUserTwo) {
        this.idUserTwo = idUserTwo;
    }



    /** Methods need it to be implemented to be able to use the class a ID for the Friendship class int
     *the space
     */

    public String toString() {
        return idUserOne + "_" + idUserTwo;
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idUserOne == null) ? 0 : idUserOne.hashCode());
        result = prime * result + ((idUserTwo == null) ? 0 : idUserTwo.hashCode());
        return result;
    }


    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FriendshipCompoundId other = (FriendshipCompoundId) obj;
        if (idUserOne == null) {
            if (other.idUserOne != null)
                return false;
        } else if (!idUserOne.equals(other.idUserOne))
            return false;
        if (idUserTwo == null) {
            if (other.idUserTwo != null)
                return false;
        } else if (!idUserTwo.equals(other.idUserTwo))
            return false;
        return true;
    }

}
