package domain;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;
import com.gigaspaces.annotation.pojo.SpaceIndex;
import com.gigaspaces.annotation.pojo.SpaceRouting;
import com.gigaspaces.metadata.index.SpaceIndexType;

import java.util.Map;

/**
 * @author Yeison Melo {@literal <mailto:melo.yeison@gmail.com/>}
 * Space
 */

@SpaceClass
public class User {
    private Long id;
    private String name;
    private String address;
    private Integer age;
    private Map<String, String> contacts;

    public User() {
    }

    public User(Long id, String name, String adress, Integer age) {
        this.id = id;
        this.name = name;
        this.address = adress;
        this.age = age;
    }

    @SpaceRouting
    @SpaceId(autoGenerate = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @SpaceIndex(type = SpaceIndexType.BASIC)
    public String getName() {
        return name;
    }

    //@SpaceIndex(type = SpaceIndexType.EXTENDED)
    public Integer getAge(){
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Map<String, String> getContacts() {
        return contacts;
    }

    public void setContacts(Map<String, String> contacts) {
        this.contacts = contacts;
    }
}
